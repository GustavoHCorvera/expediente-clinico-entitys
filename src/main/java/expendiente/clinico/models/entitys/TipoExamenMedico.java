package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoExamenMedico", schema = "catalogo")
public class TipoExamenMedico implements Serializable {

	private static final long serialVersionUID = 473914175L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoExamenMedico")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de examen médico no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreExamenMedico", unique = true)
	private String nombreExamenMedico;

	@NotNull(message = "El estado del examén médico no debe de ser null")
	@Basic(optional = false)
	@Column(name = "estadoTipoExamenMedico")
	private Boolean estadoTipoExamenMedico;

	public TipoExamenMedico(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de examen médico no puede quedar vacio") String nombreExamenMedico,
			@NotNull(message = "El estado del examén médico no debe de ser null") Boolean estadoTipoExamenMedico) {
		super();
		this.id = id;
		this.nombreExamenMedico = nombreExamenMedico;
		this.estadoTipoExamenMedico = estadoTipoExamenMedico;
	}

	public TipoExamenMedico() {
		super();
	}

}
