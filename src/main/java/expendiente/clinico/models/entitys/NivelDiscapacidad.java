package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "NivelDiscapacidad", schema = "catalogo")
public class NivelDiscapacidad implements Serializable{

	private static final long serialVersionUID = 6831330026L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdNivelDiscapacidad")
    private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
    @NotBlank(message = "El nombre de NivelDiscapacidad no puede quedar vacio")
    @Basic(optional = false)
    @Column(name = "NombreNivelDiscapacidad", unique = true)
    private String nombreNivelDiscapacidad;
    
    @NotNull(message = "El estado NivelDiscapacidad no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoNivelDiscapacidad")
	private Boolean estadoNivelDiscapacidad;

	public NivelDiscapacidad(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de NivelDiscapacidad no puede quedar vacio") String nombreNivelDiscapacidad,
			@NotNull(message = "El estado NivelDiscapacidad no puede ser null") Boolean estadoNivelDiscapacidad) {
		super();
		this.id = id;
		this.nombreNivelDiscapacidad = nombreNivelDiscapacidad;
		this.estadoNivelDiscapacidad = estadoNivelDiscapacidad;
	}

	public NivelDiscapacidad() {
		super();
	}   
    
}
