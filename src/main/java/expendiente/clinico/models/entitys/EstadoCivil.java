package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "EstadoCivil", schema = "catalogo")
public class EstadoCivil implements Serializable {

	private static final long serialVersionUID = 960471344L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdEstadoCivil")
	private Long id;

	@Pattern(regexp = "^[a-zA-Z][a-zA-Z ]{0,39}+$", message="El nombre no puede contener símbolos, números, espacios al inicio o nas de 40 caracteres")
	@NotBlank(message = "El nombre del estado civil no puede quedar vacio.")
	@Basic(optional = false)
	@Column(name = "NombreEstadoCivil", unique = true)
	private String nombreEstadoCivil;

	@NotNull(message = "El estado de estado civil no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoEstadoCivil")
	private Boolean estadoEstadoCivil;

	public EstadoCivil(Long id,
			@Pattern(regexp = "^[a-zA-Z][a-zA-Z ]{0,39}+$", message = "El nombre no puede contener símbolos, números, espacios al inicio o nas de 40 caracteres") @NotBlank(message = "El nombre del estado civil no puede quedar vacio.") String nombreEstadoCivil,
			@NotNull(message = "El estado de estado civil no puede ser null") Boolean estadoEstadoCivil) {
		super();
		this.id = id;
		this.nombreEstadoCivil = nombreEstadoCivil;
		this.estadoEstadoCivil = estadoEstadoCivil;
	}

	public EstadoCivil() {
		super();
	}

}
