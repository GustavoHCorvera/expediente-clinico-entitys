package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "PlanificacionFamiliar", schema = "catalogo")
public class PlanificacionFamiliar implements Serializable{

	private static final long serialVersionUID = 710842651L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdPlanificacionFamiliar")
    private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
    @NotBlank(message = "El nombre de la planificacion familiar no puede quedar vacio.")
    @Basic(optional = false)
    @Column(name = "NombrePlanificacionFamiliar", unique = true)
    private String nombrePlanificacionFamiliar;
   
	
    @NotNull(message = "El estado de planificacion familiar no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoPlanificacionFamiliar")
    private Boolean estadoPlanificacionFamiliar;
    
	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres")
    @NotBlank(message = "La descripcion de planificacion familiar no puede quedar vacio.")
    @Basic(optional = false)
    @Column(name = "DescripcionPlanificacionFamiliar")
    private String descripcionPlanificacionFamiliar;

	public PlanificacionFamiliar(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de la planificacion familiar no puede quedar vacio.") String nombrePlanificacionFamiliar,
			@NotNull(message = "El estado de planificacion familiar no puede ser null") Boolean estadoPlanificacionFamiliar,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres") @NotBlank(message = "La descripcion de planificacion familiar no puede quedar vacio.") String descripcionPlanificacionFamiliar) {
		super();
		this.id = id;
		this.nombrePlanificacionFamiliar = nombrePlanificacionFamiliar;
		this.estadoPlanificacionFamiliar = estadoPlanificacionFamiliar;
		this.descripcionPlanificacionFamiliar = descripcionPlanificacionFamiliar;
	}

	public PlanificacionFamiliar() {
		super();
	}

}
