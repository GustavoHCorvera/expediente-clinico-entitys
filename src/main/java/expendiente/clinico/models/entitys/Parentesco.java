package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "Parentesco", schema = "catalogo")
public class Parentesco implements Serializable {

	private static final long serialVersionUID = 653996551L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdParentesco")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de NombreParentesco no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreParentesco", unique = true)
	private String nombreParentesco;

	@NotNull(message = "El campo de EsCirculoPrincipal no puede ser null")
	@Basic(optional = false)
	@Column(name = "EsCirculoPrincipal")
	private Boolean esCirculoPrincipal;

	@Column(name = "GradoConsanguinidad")
	private Short gradoConsanguinidad;

	@NotNull(message = "El estado de Parentesco no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoParentesco")
	private Boolean estadoParentesco;

	public Parentesco(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de NombreParentesco no puede quedar vacio") String nombreParentesco,
			@NotNull(message = "El campo de EsCirculoPrincipal no puede ser null") Boolean esCirculoPrincipal,
			Short gradoConsanguinidad,
			@NotNull(message = "El estado de Parentesco no puede ser null") Boolean estadoParentesco) {
		super();
		this.id = id;
		this.nombreParentesco = nombreParentesco;
		this.esCirculoPrincipal = esCirculoPrincipal;
		this.gradoConsanguinidad = gradoConsanguinidad;
		this.estadoParentesco = estadoParentesco;
	}

	public Parentesco() {
		super();
	}

}
