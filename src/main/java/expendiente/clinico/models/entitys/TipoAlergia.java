package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "TipoAlergia", schema = "catalogo")
public class TipoAlergia implements Serializable {

	private static final long serialVersionUID = -8059923773102987535L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdTipoAlergia")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de TipoAlergia no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreTipoAlergia")
	private String nombreTipoAlergia;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres")
    @Column(name = "DescripcionTipoAlergia")
    private String descripcionTipoAlergia;
	
    @NotNull(message = "El estado de TipoAlergia no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoTipoAlergia")
    private Boolean estadoTipoAlergia;

	public TipoAlergia(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de TipoAlergia no puede quedar vacio") String nombreTipoAlergia,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,126}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 127 caracteres") String descripcionTipoAlergia,
			@NotNull(message = "El estado de TipoAlergia no puede ser null") Boolean estadoTipoAlergia) {
		super();
		this.id = id;
		this.nombreTipoAlergia = nombreTipoAlergia;
		this.descripcionTipoAlergia = descripcionTipoAlergia;
		this.estadoTipoAlergia = estadoTipoAlergia;
	}

	public TipoAlergia() {
		super();
	}
	
}
