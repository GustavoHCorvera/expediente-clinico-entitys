package expendiente.clinico.models.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.ForeignKey;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "Departamento", schema = "catalogo")
public class Departamento implements Serializable {

	private static final long serialVersionUID = 586550533L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdDepartamento")
	private Long id;

	@NotNull(message = "El estado departamento no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoDepartamento")
	private Boolean estadoDepartamento;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚ][a-zA-ZáéíóúÁÉÍÓÚ ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre del departamento no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreDepartamento", unique = true)
	private String nombreDepartamento;

	@JoinColumn(name = "IdZona", referencedColumnName = "IdZona", foreignKey=@ForeignKey(name="FK_Departamento_Zona"))
	@ManyToOne(optional = false, targetEntity = Zona.class)
	private Zona idZona;

	@JsonIgnore
	@OneToMany(mappedBy = "idDepartamento")
	private List<Municipio> municipioList;

	public Departamento(Long id,
			@NotNull(message = "El estado departamento no puede ser null") Boolean estadoDepartamento,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚ][a-zA-ZáéíóúÁÉÍÓÚ ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre del departamento no puede quedar vacio") String nombreDepartamento,
			Zona idZona) {
		super();
		this.id = id;
		this.estadoDepartamento = estadoDepartamento;
		this.nombreDepartamento = nombreDepartamento;
		this.idZona = idZona;
	}

	public Departamento() {
		super();
	}
		
}
