package expendiente.clinico.models.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "Zona", schema = "catalogo")
public class Zona implements Serializable {

	private static final long serialVersionUID = 551396039L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdZona")
	private Long id;

	@NotNull(message = "El estado zona no puede ser null")
	@Basic(optional = false)
	@Column(name = "EstadoZona")
	private Boolean estadoZona;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre de la zona no puede quedar vacio")
	@Basic(optional = false)
	@Column(name = "NombreZona", unique = true)
	private String nombreZona;

	@JsonIgnore
	@OneToMany(mappedBy = "idZona")
	private List<Departamento> departamentoList;

	public Zona(Long id, @NotNull(message = "El estado zona no puede ser null") Boolean estadoZona,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre de la zona no puede quedar vacio") String nombreZona) {
		super();
		this.id = id;
		this.estadoZona = estadoZona;
		this.nombreZona = nombreZona;
	}

	public Zona() {
		super();
	}
	
}
