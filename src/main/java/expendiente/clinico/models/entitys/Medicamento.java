package expendiente.clinico.models.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "Medicamento", schema = "catalogo")
public class Medicamento implements Serializable {

	private static final long serialVersionUID = 534340418L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdMedicamento")
	private Long id;

	@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres")
	@NotBlank(message = "El nombre del medicamento no puede quedar vacío")
	@Basic(optional = false)
	@Column(name = "NombreMedicamento", unique = true)
	private String nombreMedicamento;

	@NotNull(message = "El estado del medicamento no debe de ser null")
	@Basic(optional = false)
	@Column(name = "EstadoMedicamento")
	private Boolean estadoMedicamento;

	@JoinColumn(name = "IdTipoMedicamento", referencedColumnName = "IdTipoMedicamento", foreignKey = @ForeignKey(name = "FK_Medicamento_TipoMedicamento"))
	@ManyToOne(optional = false, targetEntity = TipoMedicamento.class)
	private TipoMedicamento idTipoMedicamento;

	public Medicamento(Long id,
			@Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890][a-zA-ZáéíóúÁÉÍÓÚñÑ1234567890., ]{0,39}+$", message = "El nombre no puede contener espacios en blanco al inicio, ni ser mayor de 40 caracteres") @NotBlank(message = "El nombre del medicamento no puede quedar vacío") String nombreMedicamento,
			@NotNull(message = "El estado del medicamento no debe de ser null") Boolean estadoMedicamento,
			TipoMedicamento idTipoMedicamento) {
		super();
		this.id = id;
		this.nombreMedicamento = nombreMedicamento;
		this.estadoMedicamento = estadoMedicamento;
		this.idTipoMedicamento = idTipoMedicamento;
	}

	public Medicamento() {
		super();
	}

}
